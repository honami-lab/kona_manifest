# Sync

```
repo init -u https://gitlab.com/honami-lab/kona_manifest -b LA.UM.9.12.r1
```

```
repo sync -c -j$(nproc --all) --no-clone-bundle --no-tags --force-sync
```

# Build
```
 . build/envsetup.sh && lunch kona-user && ./build.sh -j$(nproc --all) | tee log.xt
```
